package com.truemoney.shweyeewin.testing;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

public class TopUpViewActivity extends Activity {

    List<String> ChildList;
    Map<String, List<String>> ParentListItems;
    ExpandableListView expandablelistView;

    // Assign Parent list items here.
    List<String> ParentList = new ArrayList<String>();
    {
        ParentList.add("MPT PIN");
        ParentList.add("MPT E-Load");
        ParentList.add("Telenor PIN");
        ParentList.add("Telenor E-Load");
        ParentList.add("Ooredoo PIN");
        ParentList.add("Ooredoo E-Load");
        ParentList.add("MEC PIN");
        ParentList.add("MEC E-Load");
    }

    // Assign children list element using string array.
    String[] mpt_pin = { "1000 kyats      ၁၀၀၀ က်ပ္","3000 kyats      ၃၀၀၀ က်ပ္",
            "5000 kyats      ၅၀၀၀ က်ပ္","10000 kyats      ၁၀၀၀၀ က်ပ္" };
    String[] mpt_eload = { "500 kyats      ၅၀၀ က်ပ္","1000 kyats      ၁၀၀၀ က်ပ္","3000 kyats      ၃၀၀၀ က်ပ္",
            "5000 kyats      ၅၀၀၀ က်ပ္","10000 kyats      ၁၀၀၀၀ က်ပ္" };

    String[] telenor_pin = { "1000 kyats      ၁၀၀၀ က်ပ္","3000 kyats      ၃၀၀၀ က်ပ္",
            "5000 kyats      ၅၀၀၀ က်ပ္","10000 kyats      ၁၀၀၀၀ က်ပ္" };
    String[] telenor_eload = { "1000 kyats      ၁၀၀၀ က်ပ္","3000 kyats      ၃၀၀၀ က်ပ္",
            "5000 kyats      ၅၀၀၀ က်ပ္","10000 kyats      ၁၀၀၀၀ က်ပ္" };

    String[] ooredoo_pin = { "1000 kyats      ၁၀၀၀ က်ပ္","3000 kyats      ၃၀၀၀ က်ပ္",
            "5000 kyats      ၅၀၀၀ က်ပ္","10000 kyats      ၁၀၀၀၀ က်ပ္" };
    String[] ooredoo_eload = { "500 kyats      ၅၀၀ က်ပ္","1000 kyats      ၁၀၀၀ က်ပ္","3000 kyats      ၃၀၀၀ က်ပ္",
            "5000 kyats      ၅၀၀၀ က်ပ္","10000 kyats      ၁၀၀၀၀ က်ပ္" };

    String[] mec_pin = { "1000 kyats      ၁၀၀၀ က်ပ္","3000 kyats      ၃၀၀၀ က်ပ္",
            "5000 kyats      ၅၀၀၀ က်ပ္","10000 kyats      ၁၀၀၀၀ က်ပ္" };
    String[] mec_eload = { "1000 kyats      ၁၀၀၀ က်ပ္","3000 kyats      ၃၀၀၀ က်ပ္",
            "5000 kyats      ၅၀၀၀ က်ပ္","10000 kyats      ၁၀၀၀၀ က်ပ္" };

    String[] ByDefalutMessage = {"Items Loading ....."};

    public static final Integer[] Images = {R.drawable.mptup, R.drawable.mptup,
            R.drawable.telenorup, R.drawable.telenorup, R.drawable.ooredooup,
            R.drawable.ooredooup, R.drawable.metelup, R.drawable.metelup};

    AlertDialog.Builder alertDialogBuilder_1000, alertDialogBuilder_3000, alertDialogBuilder_5000,
            alertDialogBuilder_10000;

    AlertDialog alertDialog_1000, alertDialog_3000, alertDialog_5000, alertDialog_10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup_expand_listview);

        ParentListItems = new LinkedHashMap<String, List<String>>();

        for ( String HoldItem : ParentList) {
            if (HoldItem.equals("MPT PIN")) {
                loadChild(mpt_pin);
            } else if (HoldItem.equals("MPT E-Load")) {
                loadChild(mpt_eload);
            } else if (HoldItem.equals("Telenor PIN")) {
                loadChild(telenor_pin);
            } else if (HoldItem.equals("Telenor E-Load")) {
                loadChild(telenor_eload);
            } else if (HoldItem.equals("Ooredoo PIN")) {
                loadChild(ooredoo_pin);
            } else if (HoldItem.equals("Ooredoo E-Load")) {
                loadChild(ooredoo_eload);
            } else if (HoldItem.equals("MEC PIN")) {
                loadChild(mec_pin);
            } else if (HoldItem.equals("MEC E-Load")) {
                loadChild(mec_eload);
            } else
                loadChild(ByDefalutMessage);

            ParentListItems.put(HoldItem, ChildList);
        }

        expandablelistView = (ExpandableListView) findViewById(R.id.topup_lvExp);

        final ExpandableListAdapter expListAdapter = new ListAdapter(
                this, ParentList, ParentListItems);

        expandablelistView.setAdapter(expListAdapter);

        expandablelistView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                Intent i;

                if (groupPosition == 0) {

                    if (childPosition == 0) {

                        alertDialogBuilder_1000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_1000
                                .setMessage("Are you sure you want to buy MPT 1000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_1000 = alertDialogBuilder_1000.create();
                        alertDialog_1000.show();
                    }
                    if (childPosition == 1) {
                        alertDialogBuilder_3000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_3000
                                .setMessage("Are you sure you want to buy MPT 3000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        alertDialog_3000 = alertDialogBuilder_3000.create();
                        alertDialog_3000.show();
                    }
                    if (childPosition == 2) {
                        alertDialogBuilder_5000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_5000
                                .setMessage("Are you sure you want to buy MPT 5000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_5000 = alertDialogBuilder_5000.create();
                        alertDialog_5000.show();
                    }
                    if (childPosition == 3) {
                        alertDialogBuilder_10000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_10000
                                .setMessage("Are you sure you want to buy MPT 10000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_10000 = alertDialogBuilder_10000.create();
                        alertDialog_10000.show();
                    }
                }

                if (groupPosition == 1) {

                    if (childPosition == 0) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 1) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 2) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 3) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 4) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                }

                if (groupPosition == 2) {

                    if (childPosition == 0) {

                        alertDialogBuilder_1000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_1000
                                .setMessage("Are you sure you want to buy MPT 1000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_1000 = alertDialogBuilder_1000.create();
                        alertDialog_1000.show();
                    }
                    if (childPosition == 1) {
                        alertDialogBuilder_3000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_3000
                                .setMessage("Are you sure you want to buy MPT 3000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        alertDialog_3000 = alertDialogBuilder_3000.create();
                        alertDialog_3000.show();
                    }
                    if (childPosition == 2) {
                        alertDialogBuilder_5000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_5000
                                .setMessage("Are you sure you want to buy MPT 5000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_5000 = alertDialogBuilder_5000.create();
                        alertDialog_5000.show();
                    }
                    if (childPosition == 3) {
                        alertDialogBuilder_10000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_10000
                                .setMessage("Are you sure you want to buy MPT 10000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_10000 = alertDialogBuilder_10000.create();
                        alertDialog_10000.show();
                    }
                }

                if (groupPosition == 3) {

                    if (childPosition == 0) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 1) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 2) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 3) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 4) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }

                }

                if (groupPosition == 4) {

                    if (childPosition == 0) {

                        alertDialogBuilder_1000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_1000
                                .setMessage("Are you sure you want to buy MPT 1000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_1000 = alertDialogBuilder_1000.create();
                        alertDialog_1000.show();
                    }
                    if (childPosition == 1) {
                        alertDialogBuilder_3000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_3000
                                .setMessage("Are you sure you want to buy MPT 3000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        alertDialog_3000 = alertDialogBuilder_3000.create();
                        alertDialog_3000.show();
                    }
                    if (childPosition == 2) {
                        alertDialogBuilder_5000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_5000
                                .setMessage("Are you sure you want to buy MPT 5000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_5000 = alertDialogBuilder_5000.create();
                        alertDialog_5000.show();
                    }
                    if (childPosition == 3) {
                        alertDialogBuilder_10000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_10000
                                .setMessage("Are you sure you want to buy MPT 10000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_10000 = alertDialogBuilder_10000.create();
                        alertDialog_10000.show();
                    }
                }

                if (groupPosition == 5) {

                    if (childPosition == 0) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 1) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 2) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 3) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 4) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                }

                if (groupPosition == 6) {

                    if (childPosition == 0) {

                        alertDialogBuilder_1000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_1000
                                .setMessage("Are you sure you want to buy MPT 1000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_1000 = alertDialogBuilder_1000.create();
                        alertDialog_1000.show();
                    }
                    if (childPosition == 1) {
                        alertDialogBuilder_3000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_3000
                                .setMessage("Are you sure you want to buy MPT 3000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        alertDialog_3000 = alertDialogBuilder_3000.create();
                        alertDialog_3000.show();
                    }
                    if (childPosition == 2) {
                        alertDialogBuilder_5000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_5000
                                .setMessage("Are you sure you want to buy MPT 5000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_5000 = alertDialogBuilder_5000.create();
                        alertDialog_5000.show();
                    }
                    if (childPosition == 3) {
                        alertDialogBuilder_10000 = new AlertDialog.Builder(TopUpViewActivity.this);
                        alertDialogBuilder_10000
                                .setMessage("Are you sure you want to buy MPT 10000 Ks!")
                                .setCancelable(false)
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent intent = new Intent(TopUpViewActivity.this, TopupPinViewActivity.class);
                                                startActivity(intent);
                                            }
                                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        alertDialog_10000 = alertDialogBuilder_10000.create();
                        alertDialog_10000.show();
                    }
                }

                if (groupPosition == 7) {

                    if (childPosition == 0) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 1) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 2) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 3) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                    if (childPosition == 4) {
                        i = new Intent(TopUpViewActivity.this, TopupEloadViewActivity.class);
                        startActivity(i);
                    }
                }
                return false;
            }
        });
    }

    private void loadChild(String[] ParentElementsName) {

        ChildList = new ArrayList<String>();

        for (String model : ParentElementsName)
            ChildList.add(model);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.

            Intent intent= new Intent(TopUpViewActivity.this, LoginViewActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}