package com.truemoney.shweyeewin.testing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

public class LoginViewActivity extends Activity {

    Integer[] imageIDs = {

            R.drawable.phbill, R.drawable.billpay,
            R.drawable.transfer, R.drawable.history,
            R.drawable.setting, R.drawable.qrcode,
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);

        final Button card_btn = (Button) findViewById(R.id.card);
        /*
         */

        card_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                //card_btn.setText("testing......");
                finish();
            }
        });

        final GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new ImageAdapter(this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                Intent intent;

                String list = String.valueOf(parent.getItemAtPosition(position));

                if (position == 0) {
                    //code specific to first list item
                    LoginViewActivity.this.setContentView(R.layout.topup_expand_listview);
                    intent = new Intent(v.getContext(), TopUpViewActivity.class);
                    startActivityForResult(intent, 0);
                }

                if (position == 1) {
                    //code specific to first list item
                    LoginViewActivity.this.setContentView(R.layout.bill_view);
                    intent = new Intent(v.getContext(), BillViewActivity.class);
                    startActivityForResult(intent, 0);
                }

                if (position == 2) {
                    //code specific to first list item
                    LoginViewActivity.this.setContentView(R.layout.topup_expand_listview);
                    intent = new Intent(v.getContext(), TransferViewActivity.class);
                    startActivityForResult(intent, 0);
                }

                if (position == 3) {
                    //code specific to first list item
                    LoginViewActivity.this.setContentView(R.layout.bill_view);
                    intent = new Intent(v.getContext(), TransactionViewActivity.class);
                    startActivityForResult(intent, 0);
                }

                if (position == 4) {
                    //code specific to first list item
                    LoginViewActivity.this.setContentView(R.layout.topup_expand_listview);
                    intent = new Intent(v.getContext(), SettingViewActivity.class);
                    startActivityForResult(intent, 0);
                }

                if (position == 5) {
                    //code specific to first list item
                    LoginViewActivity.this.setContentView(R.layout.bill_view);
                    intent = new Intent(v.getContext(), QRViewActivity.class);
                    startActivityForResult(intent, 0);
                }

                /*LoginViewActivity.this.setContentView(R.layout.topup_expand_listview);
                intent= new Intent(LoginViewActivity.this, TopUpViewActivity.class);
                startActivity(intent);*/
            }
        });
    }

    public class ImageAdapter extends BaseAdapter {

        private Context context;

        public ImageAdapter(Context c)
        {
            context = c;
        }
        //---returns the number of images---
        public int getCount() {
            return imageIDs.length;
        }
        //---returns the ID of an item---
        public Object getItem(int position) {
            return position;
        }
        //---returns the ID of an item---
        public long getItemId(int position) {
            return position;
        }
        //---returns an ImageView view---
        public View getView(int position, View convertView,ViewGroup parent) {
            ImageView imageView;

            if (convertView == null) {

                imageView = new ImageView(context);
                imageView.setLayoutParams(new
                        GridView.LayoutParams(490, 490));
                imageView.setScaleType(
                        ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(4, 4, 4, 4);

            } else {
                imageView = (ImageView) convertView;
            }

            imageView.setImageResource(imageIDs[position]);
            return imageView;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.

            Intent intent= new Intent(LoginViewActivity.this, MainActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}