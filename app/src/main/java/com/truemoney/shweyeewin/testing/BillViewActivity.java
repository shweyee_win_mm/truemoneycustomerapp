package com.truemoney.shweyeewin.testing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;

public class BillViewActivity extends Activity {

    Integer[] imageIDs = {

            R.drawable.phbill, R.drawable.billpay,
    };

    PopupWindow popUp;

    Intent intent;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bill_view);

        final GridView gridView = (GridView) findViewById(R.id.bill_gridview);
        gridView.setAdapter(new BillViewActivity.ImageAdapter(this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                String list = String.valueOf(parent.getItemAtPosition(position));

                if (position == 0) {
                    //code specific to first list item

                    AeonPopupWindow(v);

                }

                if (position == 1) {
                    //code specific to first list item

                    CPPopupWindow(v);
                }
            }
        });
    }

    private void AeonPopupWindow(View parent) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.aeon_popup, null);

        popUp = new PopupWindow(v, 1500, 1100);

        popUp.showAtLocation(v, Gravity.CENTER, 0, 0);

        //BillViewActivity.this.setContentView(R.layout.aeon_popup);
        intent = new Intent(parent.getContext(), AeonPopupActivity.class);
        startActivityForResult(intent, 0);
    }

    private void CPPopupWindow(View parent) {

        BillViewActivity.this.setContentView(R.layout.topup_expand_listview);
        intent = new Intent(parent.getContext(), TopUpViewActivity.class);
        startActivityForResult(intent, 0);

    }

    public class ImageAdapter extends BaseAdapter {

        private Context context;

        public ImageAdapter(Context c)
        {
            context = c;
        }
        //---returns the number of images---
        public int getCount() {
            return imageIDs.length;
        }
        //---returns the ID of an item---
        public Object getItem(int position) {
            return position;
        }
        //---returns the ID of an item---
        public long getItemId(int position) {
            return position;
        }
        //---returns an ImageView view---
        public View getView(int position, View convertView,ViewGroup parent) {
            ImageView imageView;

            if (convertView == null) {

                imageView = new ImageView(context);
                imageView.setLayoutParams(new
                        GridView.LayoutParams(490, 490));
                imageView.setScaleType(
                        ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(4, 4, 4, 4);

            } else {

                imageView = (ImageView) convertView;
            }

            imageView.setImageResource(imageIDs[position]);

            return imageView;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.

            Intent intent= new Intent(BillViewActivity.this, LoginViewActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}