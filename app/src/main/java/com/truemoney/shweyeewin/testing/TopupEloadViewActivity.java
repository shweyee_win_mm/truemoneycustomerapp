package com.truemoney.shweyeewin.testing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TopupEloadViewActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup_eload);

        EditText topup_eload = (EditText) findViewById(R.id.topup_pin_edttext);

        Button eload_QR = (Button) findViewById(R.id.eload_qrBtn);
        Button eload_ok = (Button) findViewById(R.id.eload_okBtn);
        Button eload_cancel = (Button) findViewById(R.id.eload_cancelBtn);

        String m_PhoneNumber;

        TelephonyManager tMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        m_PhoneNumber = tMgr.getLine1Number();

        topup_eload.setText(m_PhoneNumber);

        eload_QR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        eload_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(TopupEloadViewActivity.this, TopupEloadDetailViewActivity.class);
                startActivity(intent);
            }
        });

        eload_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(TopupEloadViewActivity.this, TopUpViewActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.

            Intent intent= new Intent(TopupEloadViewActivity.this, LoginViewActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}