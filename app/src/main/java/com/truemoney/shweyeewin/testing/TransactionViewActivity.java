package com.truemoney.shweyeewin.testing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TransactionViewActivity extends Activity {

    ListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

       public static final Integer[] Images = {R.drawable.mptup, R.drawable.mptup,
            R.drawable.telenorup, R.drawable.telenorup, R.drawable.ooredooup,
            R.drawable.ooredooup, R.drawable.metelup, R.drawable.metelup};

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup_expand_listview);

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.topup_lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new ListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Child click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Child Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Child expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Child collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
    }

    /* Preparing the list data */

    private void prepareListData() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("MPT PIN");
        listDataHeader.add("MPT E-Load");
        listDataHeader.add("Telenor PIN");
        listDataHeader.add("Telenor E-Load");
        listDataHeader.add("Ooredoo PIN");
        listDataHeader.add("Ooredoo E-Load");
        listDataHeader.add("MEC PIN");
        listDataHeader.add("MEC E-Load");

        // Adding child data
        List<String> mpt_pin = new ArrayList<String>();
        mpt_pin.add("1000 kyats      ၁၀၀၀ က်ပ္");
        mpt_pin.add("3000 kyats      ၃၀၀၀ က်ပ္");
        mpt_pin.add("5000 kyats      ၅၀၀၀ က်ပ္");
        mpt_pin.add("10000 kyats    ၁၀၀၀၀ က်ပ္");

        List<String> mpt_eload = new ArrayList<String>();
        mpt_eload.add("1000 kyats      ၁၀၀၀ က်ပ္");
        mpt_eload.add("3000 kyats      ၃၀၀၀ က်ပ္");
        mpt_eload.add("5000 kyats      ၅၀၀၀ က်ပ္");
        mpt_eload.add("10000 kyats    ၁၀၀၀၀ က်ပ္");

        List<String> telenor_pin = new ArrayList<String>();
        telenor_pin.add("1000 kyats      ၁၀၀၀ က်ပ္");
        telenor_pin.add("3000 kyats      ၃၀၀၀ က်ပ္");
        telenor_pin.add("5000 kyats      ၅၀၀၀ က်ပ္");
        telenor_pin.add("10000 kyats    ၁၀၀၀၀ က်ပ္");

        List<String> telenor_eload = new ArrayList<String>();
        telenor_eload.add("1000 kyats      ၁၀၀၀ က်ပ္");
        telenor_eload.add("3000 kyats      ၃၀၀၀ က်ပ္");
        telenor_eload.add("5000 kyats      ၅၀၀၀ က်ပ္");
        telenor_eload.add("10000 kyats    ၁၀၀၀၀ က်ပ္");

        List<String> ooredoo_pin = new ArrayList<String>();
        ooredoo_pin.add("1000 kyats      ၁၀၀၀ က်ပ္");
        ooredoo_pin.add("3000 kyats      ၃၀၀၀ က်ပ္");
        ooredoo_pin.add("5000 kyats      ၅၀၀၀ က်ပ္");
        ooredoo_pin.add("10000 kyats    ၁၀၀၀၀ က်ပ္");

        List<String> ooredoo_eload = new ArrayList<String>();
        ooredoo_eload.add("1000 kyats      ၁၀၀၀ က်ပ္");
        ooredoo_eload.add("3000 kyats      ၃၀၀၀ က်ပ္");
        ooredoo_eload.add("5000 kyats      ၅၀၀၀ က်ပ္");
        ooredoo_eload.add("10000 kyats    ၁၀၀၀၀ က်ပ္");

        List<String> mec_pin = new ArrayList<String>();
        mec_pin.add("1000 kyats      ၁၀၀၀ က်ပ္");
        mec_pin.add("3000 kyats      ၃၀၀၀ က်ပ္");
        mec_pin.add("5000 kyats      ၅၀၀၀ က်ပ္");
        mec_pin.add("10000 kyats    ၁၀၀၀၀ က်ပ္");

        List<String> mec_eload = new ArrayList<String>();
        mec_eload.add("1000 kyats      ၁၀၀၀ က်ပ္");
        mec_eload.add("3000 kyats      ၃၀၀၀ က်ပ္");
        mec_eload.add("5000 kyats      ၅၀၀၀ က်ပ္");
        mec_eload.add("10000 kyats    ၁၀၀၀၀ က်ပ္");

        listDataChild.put(listDataHeader.get(0), mpt_pin);
        listDataChild.put(listDataHeader.get(1), mpt_eload);
        listDataChild.put(listDataHeader.get(2), telenor_pin);
        listDataChild.put(listDataHeader.get(3), telenor_eload);
        listDataChild.put(listDataHeader.get(4), ooredoo_pin);
        listDataChild.put(listDataHeader.get(5), ooredoo_eload);
        listDataChild.put(listDataHeader.get(6), mec_pin);
        listDataChild.put(listDataHeader.get(7), mec_eload);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.

            Intent intent= new Intent(TransactionViewActivity.this, LoginViewActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}