package com.truemoney.shweyeewin.testing;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

    String ph_number;

    //String URL = "http://10.0.3.2/test_android/index.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        final EditText card_no = (EditText) findViewById(R.id.cardno);
        EditText pin_no = (EditText) findViewById(R.id.pinno);
        EditText ph_no = (EditText) findViewById(R.id.phno);
        Button login = (Button) findViewById(R.id.loginBtn);

        getPhoneNumber();

        if (ph_number.equals(new String("")))
            ph_number = "No Number Available";

        ph_no.setText(ph_number);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.this.setContentView(R.layout.login_view);
                Intent intent = new Intent(MainActivity.this, LoginViewActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getPhoneNumber() {

        String m_PhoneNumber;

        TelephonyManager tMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        m_PhoneNumber = tMgr.getLine1Number();

        ph_number = m_PhoneNumber;
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setMessage("Are you sure you want to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}