package com.truemoney.shweyeewin.testing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

public class TopupPinViewActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup_pin);

        Button pin_ok = findViewById(R.id.okBtn);

        pin_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TopupPinViewActivity.this.setContentView(R.layout.topup_expand_listview);
                Intent intent = new Intent(TopupPinViewActivity.this, TopUpViewActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.

            Intent intent= new Intent(TopupPinViewActivity.this, TopUpViewActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}