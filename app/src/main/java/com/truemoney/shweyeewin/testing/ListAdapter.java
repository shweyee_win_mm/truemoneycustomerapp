package com.truemoney.shweyeewin.testing;

import java.util.List;
import java.util.Map;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends BaseExpandableListAdapter {

    private Activity context;
    private Map<String, List<String>> ParentListItems;
    private List<String> Items;

    public ListAdapter(Activity context, List<String> Items,
                       Map<String, List<String>> ParentListItems) {
        this.context = context;
        this.ParentListItems = ParentListItems;
        this.Items = Items;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return ParentListItems.get(Items.get(groupPosition)).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater child_inflater = context.getLayoutInflater();
            /*LayoutInflater child_inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
            convertView = child_inflater.inflate(R.layout.topup_list_item, null);
        }

        TextView txt_ListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txt_ListChild.setText(childText);
        convertView.setPadding(0, 0, 0, 20);
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return ParentListItems.get(Items.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition) {
        return Items.get(groupPosition);
    }

    public int getGroupCount() {
        return Items.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater group_inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = group_inflater.inflate(R.layout.topup_list_group, null);
        }

        TextView lbl_ListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lbl_ListHeader.setTypeface(null, Typeface.BOLD);
        lbl_ListHeader.setText(headerTitle);

        ImageView img_ListGroup= (ImageView) convertView
                .findViewById(R.id.listviewimg);

        img_ListGroup.setImageResource(TopUpViewActivity.Images[groupPosition]);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}