package com.truemoney.shweyeewin.testing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AeonPopupActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aeon_popup);

        final EditText aeon_agreement_no = (EditText) findViewById(R.id.aeon_agree_no);
        EditText aeon_name = (EditText) findViewById(R.id.aeon_name);
        EditText aeon_amt = (EditText) findViewById(R.id.aeon_amount);
        EditText aeon_ph = (EditText) findViewById(R.id.aeon_phno);
        Button ok = (Button) findViewById(R.id.okBtn);
        Button cancel = (Button) findViewById(R.id.cancelBtn);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /*AeonPopupActivity.this.setContentView(R.layout.login_view);
                Intent intent = new Intent(AeonPopupActivity.this, BillViewActivity.class);
                startActivity(intent);*/
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AeonPopupActivity.this.setContentView(R.layout.bill_view);
                Intent intent = new Intent(AeonPopupActivity.this, BillViewActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.

            Intent intent= new Intent(AeonPopupActivity.this, BillViewActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}